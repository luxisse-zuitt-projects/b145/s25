console.log('Hello');

// insertOne method

db.users.insertOne(
		{
			"firstName": "Jane",
			"lastName": "Doe",
			"age": 21,
			"contact": {
				"phone": "87654321",
				"email": "janedoe@gmail.com"
			},
			"courses": ["CSS", "Javascript", "Python"],
			"department": "none"
		}
	);

// insertMany method
// db.collections.insertMany([{doc1}, {{doc2}, ...}]);

db.users.insertMany(
  [
    {
      "firstName": "Stephen",
      "lastName": "Hawking",
      "age": 76,
      "contact": {
        "phone": "87654321",
        "email": "stephenhawking@gmail.com"
      },
      "courses": ["Python", "React", "PHP"],
      "department": "none"
      }, 
		{
      "firstName": "Neil",
      "lastName": "Armstrong",
      "age": 82,
      "contact": {
        "phone": "87654321",
        "email": "neilarmstrong@gmail.com"
    },
      "courses": ["React", "Laravel", "Sass"],
      "department": "none"
    }, 
  ]
);

// Read Operation
	// - retrieves documents from the collection

	db.collections.find({query}, {field projection})

	// find() method
	db.users.find();


// Update Operation
	// - update a document/s

	db.collections.updateOne({fiter}, {update})
	db.collections.updateMany()

	// add first a document to be modified
	db.users.insertOne(
		{
			"firstName": "Test",
      "lastName": "Test",
      "age": 0,
      "contact": {
        "phone": "0",
        "email": "test@gmail.com"
    	},
      "courses": [],
      "department": "none"
		}
	);

	// modifying the document using //updatOne() method
	db.users.updateOne(
		{"firstName" : "Test"},
		{
			$set: {
					"firstName": "Bill",
		      "lastName": "Gates",
		      "age": 65,
		      "contact": {
		        "phone": "12345678",
		        "email": "bill@gmail.com"
		    	},
		      "courses": ["PHP", "Laravel", "HTML"],
		      "department": "operations",
		      "status": "active"
			}
		}
	);

	// use first name field as a filter and look for the name test
	// using update operator set, update the fields of the matching document with the following details
		// bill gates, 65yo, phone: 12345678, bill@gmail.com, courses PHP, Laravel, HTML, operations dept, status: active


	// updateMany() method
	db.users.updateMany(
    {"department" : "none"},
			{
        $set: {
          "department": "HR"
      }
		}
	);


	// Mini Activity
		// look for a document that has a field status using object id as a filter
		// remove the status field using update operator

	db.users.updateOne(
    {"_id" : ObjectId("61e7fd7efe6eed56d71d29ac")},
			{
        $unset: {
          "status": "active"
      }
		}
	);

// Delete Operation
	//  - delete a document/s

	db.collections.deleteOne({filter})
	db.collections.deleteMany({filter})

	// insert a document as an example to be deleted
	db.users.insertOne(
		{
			"firstName": "Joy",
      "lastName": "Pague"
		}
	);

	// deleteOne() method
	db.users.deleteOne(
		{
			"firstName": "Joy",
		}
	);

	// insert a document as an example to be deleted
	db.users.insertOne({"firstName": "Bill", "lastName": "Crawford"});

	// deleteMany() method
	db.users.deleteMany(
		{"firstName": "Bill"}
	);

// Activity






